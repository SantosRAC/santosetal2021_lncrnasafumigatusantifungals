#!/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Software/anaconda3/bin/python3

import argparse
import sys
import pandas as pd

# Arguments that can be indicated by users
version=sys.argv[0] + ' ' + "0.1"
parser = argparse.ArgumentParser(description='Identify genes that are always turned on or off based on a threshold', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-i','--input_matrix', dest='inGXMATRIX', metavar='Gene Expression Matrix (TMM)', type=str, help='Alignment file', required=True)

# Parsing command-line arguments
args = parser.parse_args()
matrixFile = args.inGXMATRIX

# Just assigning conditions of each BIOPROJECT to lists; they will be used to filter pandas DataFrames below
oneCondition1 = ['KU80_1','KU80_2','KU80_3']
secCondition1 = ['KU80_VCZ_1','KU80_VCZ_2','KU80_VCZ_3']
oneCondition2 = ['A1160ctl_exp2_1','A1160ctl_exp2_2','A1160ctl_exp2_3']
secCondition2 = ['A1160ITC_exp2_1','A1160ITC_exp2_2','A1160ITC_exp2_3']
oneCondition3 = ['A1160_control_1','A1160_control_2','A1160_control_3']
secCondition3 = ['A1160_ITC_1','A1160_ITC_2','A1160_ITC_3']
allConditions = oneCondition1 + secCondition1 + oneCondition2 + secCondition2 + oneCondition3 + secCondition3

# Importing the file with TMM values and assigning it to a pandas DataFrame
matrixDF = pd.read_csv(matrixFile,sep="\t")

# Print the comparison of only just one BIOPROJECT
#print(matrixDF[(matrixDF[oneCondition].mean(axis=1) > 5) & (matrixDF[secCondition].mean(axis=1) < 1)][['ID','KU80_VCZ_1','KU80_VCZ_2','KU80_VCZ_3','KU80_1','KU80_2','KU80_3']])

# Print the results of multiple logical operators (overexpressed in drugs)
print(matrixDF[((matrixDF[oneCondition1].mean(axis=1) < 1) & (matrixDF[secCondition1].mean(axis=1) > 3)) & ((matrixDF[oneCondition2].mean(axis=1) < 1) & (matrixDF[secCondition2].mean(axis=1) > 3)) & ((matrixDF[oneCondition3].mean(axis=1) < 1) & (matrixDF[secCondition3].mean(axis=1) > 3))][['ID']+allConditions])
# Print the results of multiple logical operators (downexpressed in drugs)
print(matrixDF[((matrixDF[oneCondition1].mean(axis=1) > 5) & (matrixDF[secCondition1].mean(axis=1) < 2)) & ((matrixDF[oneCondition2].mean(axis=1) > 5) & (matrixDF[secCondition2].mean(axis=1) < 2)) & ((matrixDF[oneCondition3].mean(axis=1) > 5) & (matrixDF[secCondition3].mean(axis=1) < 2))][['ID']+allConditions])
