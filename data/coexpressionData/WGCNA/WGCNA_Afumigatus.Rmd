---
title: "R Notebook"
output:
  pdf_document: default
  html_notebook: default
---

# Weighted gene correlation network analysis (WGCNA)

## Running analysis with example dataset (online tutorial)


```{r}
setwd("/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Projects/UNICAMP/LGE/CoExpressionAnalyses/WGCNA")

# Importing WGCNA
library(WGCNA)

# Importing the example data provided by the tutorial (WGCNA webpage)
femData = read.csv("TestData/LiverFemale3600.csv")

# Checking how data is
dim(femData)
names(femData)
head(femData)

# Converting the original data to a more suitable for WGCNA ()
dataExprO = as.data.frame(t(femData[,-c(1:8)]))
names(dataExprO) = femData$substanceBXH
names(dataExprO)
rownames(dataExprO) = names(femData[-c(1:8)])
dataExprO
```

## Running real analysis (A. fumigatus)

### Data import and cleaning

Using the TMM matrix

```{r}
# Set the correct directory
setwd("/media/renato/718c122e-ff56-4ee9-85ec-00601685cd39/Projects/UNICAMP/LGE/CoExpressionAnalyses/WGCNA")
getwd()

# Importing WGCNA
library(WGCNA)

# Importing the gene expression matrix - the same used in clust (TMM values)
afumData <- read.table("../Results_05_Jul_21/Input_files_and_params/Data/expression2clust.txt", header = TRUE)

# Taking a look at this data set
dim(afumData)
names(afumData)

# Generating a data.frame that is suitable for WGCNA
afumData
dataExprAfum <- as.data.frame(t(afumData[,-1]))
dim(dataExprAfum)
names(dataExprAfum)
names(dataExprAfum) = afumData$ID
names(dataExprAfum)

# Checking data for excessive missing data/ identification of outliers
gsg = goodSamplesGenes(dataExprAfum, verbose = 3)
gsg$allOK

if(!gsg$allOK){
  if(sum(!gsg$goodGenes)>0)
    printFlush(paste("Removing genes:", paste(names(dataExprAfum)[!gsg$goodGenes], collapse = ", ")));
  if(sum(!gsg$goodSamples>0))
    printFlush(paste("Removing samples:", paste(rownames(dataExprAfum)[!gsg$goodSamples, collapse = ", "])));
  dataExprAfum = dataExprAfum[gsg$goodSamples, gsg$goodGenes]
}

dim(dataExprAfum)

```

### Plotting samples

```{r}
sampleTree = hclust(dist(dataExprAfum), method="average")
sampleTree

sizeGrWindow(12,9)
pdf("SampleClustering.pdf")
par(cex = 0.6)
par(mar = c(0,4,2,0))
plot(sampleTree, main="Sample clustering to detect outliers", sub="", xlab="", cex.lab=1.5, cex.axis = 1.5, cex.main=2)
while (!is.null(dev.list()))  dev.off()
```

### Choosing the power

```{r}
head(dataExprAfum)

# choose a set of soft-thresholding powers
powers = c(c(1:10), seq(from = 12, to=20, by=2))
powers

# Call the network topology analysis function
sft = pickSoftThreshold(dataExprAfum, powerVector = powers, verbose = 5)
sft

# 
sizeGrWindow(9,5)
par(mfrow = c(1,2));
cex1 = 0.9;

#
pdf("plotPowerAnalyses_Afum_A.pdf")
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2], xlab="Soft Threshold (power)", ylab="Scale Free Topology Model Fit,signed R^2",
     type="n", main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],labels=powers,cex=cex1,col="red");
#
abline(h=0.90,col="red")
dev.off()

#
pdf("plotPowerAnalyses_Afum_B.pdf")
plot(sft$fitIndices[,1], sft$fitIndices[,5],xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
dev.off()
```


### Co-expression similarity and adjacency using power 12


```{r}
softPower = 12
adjacency = adjacency(dataExprAfum, type="signed", power = softPower);
head(adjacency)
```

### Topological Overlap Matrix (TOM)

```{r}
TOM = TOMsimilarity(adjacency)
dissTOM = 1-TOM

#call the hierarchical clustering function
geneTree = hclust(as.dist(dissTOM), method="average")
geneTree

#plot the resulting clustering tree (dendrogram)
sizeGrWindow(15,9)
plot(geneTree, xlab="", sub="", main="Gene clustering on TOM-based similarity", labels=FALSE, hang=0.04)
```


```{r}
# We like larger modules, so we set the minimum module size relatively high
minModuleSize = 30

# Module identification using dynamic tree cut:
dynamicMods = cutreeDynamic(dendro = geneTree, distM = dissTOM, deepSplit = 2, pamRespectsDendro = FALSE, minClusterSize = minModuleSize)
table(dynamicMods)
```

```{r}
dynamicColors = labels2colors(dynamicMods)
table(dynamicColors)

sizeGrWindow(8,6)
plotDendroAndColors(geneTree, dynamicColors, "Dynamic Tree Cut", dendroLabels = FALSE, hang=0.03, addGuide = TRUE, guideHang = 0.05, main = "Gene dendrogram and module colors")
```

### Merging of modules whose expression profiles are very similar

```{r}
MEList = moduleEigengenes(dataExprAfum, colors = dynamicColors)
MEs = MEList$eigengenes

MEDiss = 1-cor(MEs)

METree = hclust(as.dist(MEDiss), method="average")
sizeGrWindow(7,6)
plot(METree, main="Clustering of module eigengenes", xlab="", sub="")
```

```{r}
MEDissThres = 0.25
abline(h=MEDissThres, col="red")

merge=mergeCloseModules(dataExprAfum, dynamicColors, cutHeight = MEDissThres, verbose=3)

mergedColors = merge$colors

mergedMEs = merge$newMEs

sizeGrWindow(12, 9)

plotDendroAndColors(geneTree, cbind(dynamicColors, mergedColors),c("Dynamic Tree Cut", "Merged dynamic"),dendroLabels = FALSE, hang = 0.03,addGuide = TRUE, guideHang = 0.05)
```

```{r}
# Rename to moduleColors
moduleColors = mergedColors

# Construct numerical labels corresponding to the colors
colorOrder = c("grey", standardColors(50));
moduleLabels = match(moduleColors, colorOrder)-1;
moduleLabels
unique(moduleColors)
MEs = mergedMEs;

# Save module colors and labels for use in subsequent parts
```

### Visualizing the gene network

```{r}
dissTOM = 1-TOMsimilarityFromExpr(dataExprAfum, power = 12);
plotTOM = dissTOM^13;
diag(plotTOM) = NA;
sizeGrWindow(9,9)
TOMplot(plotTOM, geneTree, moduleColors, main = "Network heatmap plot, all genes")
```

### Exporting modules (and their genes)

```{r}
unique(moduleColors)
# Recovering the genes in each module
for (module in unique(moduleColors)) {
  fileName = paste("Modules/Module_",module,".txt", sep="")
  write.table(as.data.frame(c(module,names(dataExprAfum)[moduleColors==module])),file=fileName, quote=FALSE, sep="\t")
}
```

### Exporting modules (and their genes) to analyze in Cytoscape

```{r}
for (module in unique(moduleColors)) {
 probes = names(dataExprAfum)
 inModule = is.finite(match(moduleColors,module))
 inModule
 modProbes = probes[inModule]
 modProbes
 modTOM = TOM[inModule,inModule]
 dimnames(modTOM) = list(modProbes, modProbes)
 modTOM
 cyt = exportNetworkToCytoscape(modTOM,
                               edgeFile = paste("Cytoscape/CytoscapeInput-edges-",module,".txt", sep=""),
                               nodeFile = paste("Cytoscape/CytoscapeInput-nodes-",module,".txt", sep=""),
                               weighted = TRUE,
                               threshold = 0.02,
                               nodeNames = modProbes,
                               nodeAttr = moduleColors[inModule])
}
```



